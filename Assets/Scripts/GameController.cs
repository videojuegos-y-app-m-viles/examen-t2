using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameController : MonoBehaviour
{

    public Text enemyText;
    public Text LifesText;

    private int _score = 0;
    private int _lifesPlayer;
    private int _lifesEnemy;
    private int numEnemyes;
    // Start is called before the first frame update
    void Start()
    {
        _lifesEnemy = 3;
        _lifesPlayer = 3;
        _score = 0;
        numEnemyes = 5;
        PrintLifes();
        PrintEnemyes();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public int getVidaEnemy(){
        return _lifesEnemy;
    }
    public int getVidaPlayer(){
        return _lifesPlayer;
    }
    public void restarVidaEnemy(){
        _lifesEnemy--;
        calculaEnemyes();
    }
    public void restarVideEnemyDoble(){
        _lifesEnemy -= 2;
        calculaEnemyes();
    }
    public void restarVidaPlayer(){
        _lifesPlayer--;
        PrintLifes();
    }
    private void PrintLifes()
    {
        var text = "Lives: ";
        for (var i = 0; i < _lifesPlayer; i++)
        {
            text += "I ";
        }
        
        LifesText.text = text;
    }
    private void PrintEnemyes()
    {
        var text = "Enemigos: ";
        for (var i = 0; i < numEnemyes; i++)
        {
            text += "I ";
        }
        
        enemyText.text = text;
    }
    private void calculaEnemyes(){
        if(_lifesEnemy <= 0){
            numEnemyes--;
            _lifesEnemy = 3;
            PrintEnemyes();
        }
    }
}
