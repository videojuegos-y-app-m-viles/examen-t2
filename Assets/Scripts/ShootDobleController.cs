using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootDobleController : MonoBehaviour
{
    public float velocityX = 10f;

    private const string ENEMY_TAG = "Enemy"; 

    private Rigidbody2D rb;
    private GameController _game;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        _game = FindObjectOfType<GameController>();
        Destroy(this.gameObject, 3);
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(velocityX, rb.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D  collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(gameObject);
           _game.restarVideEnemyDoble();
            Debug.Log("Vida enemigo:" + _game.getVidaEnemy());
            if(_game.getVidaEnemy() <= 0){
                Destroy(collision.gameObject);
                Debug.Log("Vida enemigo en muerto:" + _game.getVidaEnemy());
            }
        }
    }
}
