using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    // Start is called before the first frame update
    
    private Rigidbody2D rb;
    private GameController _game;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        _game = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(-5, 0);
    }
    /*
    private void OnCollisionEnter2D(Collision2D  collision)
    {
        Debug.Log("Colisión" + collision.gameObject);
        if (collision.gameObject.CompareTag("Player"))
        {
            _game.restarVidaPlayer();
            if(_game.getVidaPlayer() <= 0)
            {
                Destroy(collision.gameObject);
                //SceneManager.LoadScene("nombreEcena");
            }
        }
    }
    */
}
