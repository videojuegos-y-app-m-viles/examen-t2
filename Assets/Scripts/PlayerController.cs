using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    //gravity = 10
    // public properties
    public float velocityX = 20;
    public float jumpForce = 400;

    public GameObject rightBullet;
    public GameObject leftBullet;
    public GameObject rightDobleBullet;
    public GameObject leftDobleBullet;

    // private components
    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private Animator animator; 
    private GameController _game;

        // constants
    private const int ANIMATION_IDLE = 0;
    private const int ANIMATION_SLIDE= 1;
    private const int ANIMATION_RUN = 2;
    private const int ANIMATION_DEAD= 3;
    private const int ANIMATION_SHOOT = 4;
    private const int ANIMATION_RUNSHOOT = 5;
    private const int ANIMATION_JUMP = 6;

    //private values
    private float changeTime;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        _game = FindObjectOfType<GameController>();
        changeTime = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        
        rb.velocity = new Vector2(0, rb.velocity.y);
        changeAnimation(ANIMATION_IDLE);
        
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(velocityX, rb.velocity.y); 
            sr.flipX = false;
            changeAnimation(ANIMATION_RUN);
        }
        
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-velocityX, rb.velocity.y);
            sr.flipX = true;
            changeAnimation(ANIMATION_RUN);
        }
        changeTime += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.C))
        {
            changeTime += Time.deltaTime;
            Debug.Log("Atacando");
            Debug.Log("changeTime"+ changeTime);
            
            if(changeTime < 1){
                var bullet = sr.flipX ? leftBullet : rightBullet;
                var position = new Vector2(transform.position.x +2, transform.position.y);
                var rotation = rightBullet.transform.rotation;
                Instantiate(bullet, position, rotation);
                changeTime = 0;
            }
            else if(changeTime >= 3){
                var bullet = sr.flipX ? leftDobleBullet : rightDobleBullet;
                var position = new Vector2(transform.position.x +2, transform.position.y);
                var rotation = rightBullet.transform.rotation;
                Instantiate(bullet, position, rotation);
                changeTime = 0;
            }
            
        }
        
        if (Input.GetKey(KeyCode.X))
        {
            changeAnimation(ANIMATION_SLIDE);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse); // salta
            changeAnimation(ANIMATION_JUMP); // saltar
        }
    }
    private void OnCollisionEnter2D(Collision2D  collision)
    {
        Debug.Log("Colisión" + collision.gameObject);
        while (collision.gameObject.CompareTag("Enemy") && _game.getVidaPlayer() > 0)
        {
             _game.restarVidaPlayer();
        }
        if (_game.getVidaPlayer() <= 0)
        {
            _game.restarVidaPlayer();
            if(_game.getVidaPlayer() <= 0)
            {
                Destroy(this.gameObject);
                SceneManager.LoadScene("SampleScene");
            }
        }
        Debug.Log("Colisión" + collision.gameObject);
        if (collision.gameObject.CompareTag("Key"))
        {
            Debug.Log("Cambio de escena");
              //cambio de excena
            SceneManager.LoadScene("segundaEscena");
             
        }
         if (collision.gameObject.CompareTag("KeyFinal"))
        {
            Debug.Log("Cambio de escena");
              //cambio de excena
            SceneManager.LoadScene("SampleScenne");
             
        }
    }
        
    private void changeAnimation(int animation)
    {
        animator.SetInteger("estado", animation);
    }
}
